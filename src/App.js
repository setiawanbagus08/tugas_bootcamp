import React, { useState } from 'react';
import { Routes, Route } from "react-router-dom";
import Navbar from './components/Navbar';
import User from './components/User';
import About from './components/About';
import Hobby from './components/Hobby';
import View from './components/View';
import LandingPage from './components/LandingPage';
import UserList from './components/UserList';
import { AuthProvider } from './contexts/AuthContext';
import Login from './components/Login';
import { CommonProvider } from './contexts/CommonContext';
import NavbarNew from './components/NavbarNew';
import Page1 from './components/Page1';
import Page2 from './components/Page2';

function App() {
  const [users, setUsers] = useState([]);
  const [nextID, setNextID] = useState(1);

  const handleSaveUser = (user) => {
    setUsers([...users, { ...user, id: nextID }]);
    setNextID(nextID + 1);
  };

  const handleEditUser = (editedUser) => {
    setUsers((prevUsers) =>
      prevUsers.map((user) => (user.id === editedUser.id ? editedUser : user))
    );
  };

  console.log(users.id)

  return (
    // <Routes>
      
    //     <Route path='/' element={<Navbar onSave={handleSaveUser} />}>
    //       <Route index element={<User users={users} onEdit={handleEditUser} />} />       
    //       <Route path="/about" element={<About />} />
    //       <Route path="/hobby" element={<Hobby />} />
    //       <Route path="/landing" element={<LandingPage />} />
    //       <Route path="/view/:id" element={<View />} />
    //     </Route>
      
    // </Routes>
    // <div>
    //     <UserList name={"Bagus"} age={20}/> 
    // </div>
    <CommonProvider>
      <AuthProvider>
        <NavbarNew />
        <Routes>
          <Route path='/' element={<Login />}></Route>
          <Route path='/page1' element={<Page1 />}></Route>
          <Route path='page2' element={<Page2 />}></Route>
        </Routes>
        {/* <Login /> */}
      </AuthProvider>
    </CommonProvider>
  );
}

export default App;
