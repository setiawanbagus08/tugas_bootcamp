import React from 'react';
import { Box, Typography } from "@mui/material";

const About = () => {
    return (
        <Box sx={{ width: '500px', ml: 'auto', mr: 'auto',  mt: 10, display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
            <Typography sx={{ fontSize: '70px'}}>About Me</Typography>
            <Typography sx={{ fontSize: '40px' , textAlign: 'center'}}>Halo, namaku Bagus Setiawan.</Typography>
            <Typography sx={{ fontSize: '40px' }}>Salam kenal</Typography>
        </Box>
    );
};

export default About;