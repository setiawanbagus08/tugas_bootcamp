import { Button, Typography, Box } from "@mui/material";
import { useState } from "react";

function Counter () {
    const [counter, setCounter] = useState(0);


    return (
        <>
        <Box>
            <Button>-</Button>
            <Typography>0</Typography>
            <Button>+</Button>
        </Box>
        </>
    )
}

export default Counter;