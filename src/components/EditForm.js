import React, { useState } from 'react';
import { Button, Modal, TextField, Box, Typography, Stack } from '@mui/material';

const EditForm = ({ user, onSave }) => {
    const [showModal, setShowModal] = useState(false);
    const [name, setName] = useState(user.name);
    const [address, setAddress] = useState(user.address);
    const [hobby, setHobby] = useState(user.hobby);
    const [error, setError] = useState("");

    const handleSubmit = (e) => {
        e.preventDefault();

        if (name !== "" && address !== "" && hobby !== "") {
            onSave({ id: user.id, name, address, hobby });
            setError("");
            setShowModal(false);
        } else {
            setError("Masukkan data");
        };
    };

    const style = {
        position: "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)",
        width: 400,
        bgcolor: "background.paper",
        border: "2px solid #000",
        boxShadow: 24,
        p: 4
    };

    return (
        <div>
            <Button variant="outlined" color="primary" onClick={() => setShowModal(true)}>
                Edit
            </Button>

            <Modal open={showModal} onClose={() => setShowModal(false)}>
                <Box sx={style}>
                    <form onSubmit={handleSubmit}>
                        <Stack spacing={2}>
                            <div>
                                <Typography id="modal-modal-title" variant="h6" component="h2">Edit User</Typography>
                            </div>
                            <div>
                                <Typography id="modal-modal-description" sx={{ mt: 1 }}>
                                    Name
                                </Typography>
                                <div style={{ marginTop: '5px' }}>
                                    <TextField
                                        size="small"
                                        variant="outlined"
                                        value={name}
                                        setError=""
                                        onChange={(e) => {
                                            setName(e.target.value);
                                        }}
                                    />
                                </div>
                                {error && !name.length ? (
                                    <div>
                                        <p style={{ color: "red" }}>{error}</p>
                                    </div>
                                ) : null}
                            </div>
                            <div>
                                <Typography id="modal-modal-description" sx={{ mt: 1 }}>
                                    Address
                                </Typography>
                                <div style={{ marginTop: '5px' }}>
                                    <TextField
                                        size="small"
                                        variant="outlined"
                                        value={address}
                                        setError=""
                                        onChange={(e) => {
                                            setAddress(e.target.value);
                                        }}
                                    />
                                </div>
                                {error && !address.length ? (
                                    <div>
                                        <p style={{ color: "red" }}>{error}</p>
                                    </div>
                                ) : null}
                            </div>
                            <div>
                                <Typography id="modal-modal-description" sx={{ mt: 1 }}>
                                    Hobby
                                </Typography>
                                <div style={{ marginTop: '5px' }}>
                                    <TextField
                                        size="small"
                                        variant="outlined"
                                        value={hobby}
                                        setError=""
                                        onChange={(e) => {
                                            setHobby(e.target.value);
                                        }}
                                    />
                                </div>
                                {error && !hobby.length ? (
                                    <div>
                                        <p style={{ color: "red" }}>{error}</p>
                                    </div>
                                ) : null}
                            </div>
                            <div style={{ marginTop: "20px" }}>
                                <Button variant="contained" color="success" type="submit">Save</Button>
                            </div>
                        </Stack>
                    </form>
                </Box>
            </Modal>
        </div>
    );
};

export default EditForm;