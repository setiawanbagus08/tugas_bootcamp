import React from 'react'
import { Box, Grid, Input, TextField, Typography } from "@mui/material";

const Hobby = () => {
    const dataList = [
        {
            id: 1,
            name: "andi",
            hobby: 'Makan',
            address: 'Jember'
        },
        {
            id: 2,
            name: "rahmat",
            hobby: 'Nonton Drakor',
            address: 'Jember'
        },
        {
            id: 3,
            name: "ratu",
            hobby: 'Membaca Novel',
            address: 'Jember'
        }
    ];

    return (
        <Box sx={{ width: '500px', ml: 'auto', mr: 'auto', mt: 10, display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
            <Typography sx={{ fontSize: '50px', textAlign: 'center', fontWeight: 'bold' , mb: '30px'}}>Daftar Hobi</Typography>
            {dataList.map((item) => {
                return <Typography key={item.id} sx={{ fontSize: '40px', textAlign: 'center', }}>{item.hobby}</Typography>
            })}
        </Box>
    );
};

export default Hobby;