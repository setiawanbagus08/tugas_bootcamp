import React, {useState, useEffect} from 'react';
import { Box, Grid, useTheme, useMediaQuery, List, ListItem, ListItemIcon, ListItemText, Typography, Divider } from '@mui/material';
import { AccountCircle as AccountCircleIcon } from '@mui/icons-material';
import { createTheme } from './Theme';
import MyCard from './MyCard';
import useFetchData from '../hooks/useFetchData';

const LandingPage = () => {
    const theme = useTheme();
    const isViewportGreaterThan900px = useMediaQuery(theme.breakpoints.up('md'));
    const themes = createTheme();

    const landingPageStyle = {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: themes.primaryColor,
        color: 'white',
        height: '200px',
    };

    const { data, loading, error } = useFetchData(`${process.env.REACT_APP_BASE_URL}/posts`);

    if (loading) {
        return <div>Loading...</div>;
    }

    if (error) {
        return <div>Error fetching data: {error.message}</div>;
    }
    
    console.log(process.env.REACT_APP_COLOR_PRIMARY);

    return(
        <Box sx={{ maxWidth: '1200px', marginLeft: 'auto', marginRight: 'auto'}}>
            <Box sx={landingPageStyle}>
                <div>
                    <h1>Welcome to My Landing Page</h1>
                    <p>Hello World</p>
                </div>
            </Box>
            <Grid container >
                <Grid item xs={9}>
                    <Box display="flex" justifyContent="center" my={4}>
                        <Grid container spacing={2}>
                            {data.map((item, index) => (
                                <Grid key={index} item xs={12} sm={6}>
                                    <MyCard item={item} />
                                </Grid>
                            ))}
                        </Grid>                    
                    </Box>
                </Grid>
                {isViewportGreaterThan900px &&(
                    <Grid item xs={3}>
                        <Grid container spacing={2}>
                            <Box display="flex" flexDirection="column" justifyContent="center" my={6}>
                                <List component="nav">
                                    {data.map((item) => (
                                        <React.Fragment key={item.id}>
                                            <ListItem>
                                                <ListItemIcon>
                                                    <AccountCircleIcon />
                                                </ListItemIcon>
                                                <ListItemText
                                                    primary={item.title}
                                                    secondary={<Typography variant="body2">{item.author}</Typography>}
                                                />
                                            </ListItem>
                                            <Divider />
                                        </React.Fragment>
                                    ))}
                                </List>
                            </Box>
                        </Grid>
                    </Grid>
                )}
            </Grid>
        </Box>
    );
};

export default LandingPage;