import React, { useState, useEffect } from 'react';
import { useAuthContext } from '../contexts/AuthContext';
import { useCommonContext } from '../contexts/CommonContext'; 
import { useNavigate } from 'react-router-dom'

const Login = () => {
    const navigate = useNavigate();
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const { state, setState } = useCommonContext();

    const { isLoggedIn, login, logout } = useAuthContext();

    useEffect(() => {
        setState({ title: 'Login', color: 'red' });
    }, [setState]);

    const handleLogin = () => {
        if (username === 'demo' && password === 'password') {
            login();
        } else {
            alert('Invalid username or password');
        }
    };

    const handleLogout = () => {
        logout();
    };

    useEffect(() => {
        if (isLoggedIn) {
            navigate('/page1')
        }
    }, [isLoggedIn])

    return (
        <div>
            {isLoggedIn ? (
                <div>
                    <button onClick={handleLogout}>Logout</button>
                </div>
            ) : (
                <div>
                    <h2>Login</h2>
                    <div>
                        <label htmlFor="username">Username:</label>
                        <input
                            type="text"
                            id="username"
                            value={username}
                            onChange={(e) => setUsername(e.target.value)}
                        />
                    </div>
                    <div>
                        <label htmlFor="password">Password:</label>
                        <input
                            type="password"
                            id="password"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                        />
                    </div>
                    <button onClick={handleLogin}>Login</button>
                </div>
            )}
        </div>
    );
};

export default Login;
