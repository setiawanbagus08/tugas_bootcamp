import React, { useState } from "react";
import {Outlet, Link} from "react-router-dom";
import {
    Button,
    Typography,
    TextField,
    Box,
    Modal,
    Stack,
    AppBar,
    Toolbar
} from "@mui/material";

const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4
};


const Navbar = ({ onSave }) => {
    const [open, setOpen] = React.useState(false);
    const [name, setName] = useState('');
    const [address, setAddress] = useState('');
    const [hobby, setHobby] = useState('');
    const [error, setError] = useState("");
    const handleOpen = () => setOpen(true);
    const handleClose = () => {
        setName('');
        setAddress('');
        setHobby('');
        setOpen(false);
    }

    const handleClick = (e) => {
        e.preventDefault();
        if (name !== "" && address !== "" && hobby !== "") {
            onSave({ name, address, hobby });
            setError("");
            handleClose();
        } else {
            setError("Masukkan data");
        };
    }

    console.log(name);
    console.log(address);
    console.log(hobby);
    return (
        <Box sx={{
            height: 70,
            width: '100%',
            backgroundColor: 'primary.main'
        }}>
            <AppBar position="static" elevation={0} sx={{
                height: 86,
                width: '100%',
                backgroundColor: 'primary.main',
            }}>
                <Toolbar sx={{
                    marginTop: 'auto',
                    marginBottom: 'auto',
                    padding: '0'
                }}>
                    <Typography component={Link} to="/" sx={{ flexGrow: 1, fontSize: '30px ', fontWeight: 'bold', color: 'white' }}>
                        My App
                    </Typography>
                    <Typography component={Link} to="/about"  sx={{ flexGrow: 1, fontSize: '20px ', fontWeight: 'bold', color: 'white' }}>
                        About
                    </Typography>
                    <Typography component={Link} to="/hobby" sx={{ flexGrow: 1, fontSize: '20px ', fontWeight: 'bold', color: 'white' }}>
                        Hobby
                    </Typography>
                    <Typography component={Link} to="/landing" sx={{ flexGrow: 1, fontSize: '20px ', fontWeight: 'bold', color: 'white' }}>
                        Landing
                    </Typography>
                    
                    <Button variant="contained" color="success" onClick={handleOpen}>Add User</Button>
                    <Modal
                        open={open}
                        onClose={handleClose}
                        aria-labelledby="modal-modal-title"
                        aria-describedby="modal-modal-description"
                    >
                        <Box sx={style}>
                            <form onSubmit={handleClick}>
                                <Stack spacing={2}>
                                    <div>
                                        <Typography id="modal-modal-title" variant="h6" component="h2">
                                            Add User
                                        </Typography>
                                    </div>
                                    <div>
                                        <Typography id="modal-modal-description" sx={{ mt: 1 }}>
                                            Name
                                        </Typography>
                                        <div style={{ marginTop: '5px' }}>
                                            <TextField
                                                size="small"
                                                variant="outlined"
                                                value={name}
                                                setError=""
                                                onChange={(e) => {
                                                    setName(e.target.value);
                                                }}
                                            />
                                        </div>
                                        {error && !name.length ? (
                                            <div>
                                                <p style={{ color: "red" }}>{error}</p>
                                            </div>
                                        ) : null}
                                    </div>
                                    <div>
                                        <Typography id="modal-modal-description" sx={{ mt: 1 }}>
                                            Address
                                        </Typography>
                                        <div style={{ marginTop: '5px' }}>
                                            <TextField
                                                size="small"
                                                variant="outlined"
                                                value={address}
                                                setError=""
                                                onChange={(e) => {
                                                    setAddress(e.target.value);
                                                }}
                                            />
                                        </div>
                                        {error && !address.length ? (
                                            <div>
                                                <p style={{ color: "red" }}>{error}</p>
                                            </div>
                                        ) : null}
                                    </div>
                                    <div>
                                        <Typography id="modal-modal-description" sx={{ mt: 1 }}>
                                            Hobby
                                        </Typography>
                                        <div style={{ marginTop: '5px' }}>
                                            <TextField
                                                size="small"
                                                variant="outlined"
                                                value={hobby}
                                                setError=""
                                                onChange={(e) => {
                                                    setHobby(e.target.value);
                                                }}
                                            />
                                        </div>
                                        {error && !hobby.length ? (
                                            <div>
                                                <p style={{ color: "red" }}>{error}</p>
                                            </div>
                                        ) : null}
                                    </div>
                                    <div style={{ marginTop: "20px" }}>
                                        <Button variant="contained" color="success" type="submit">Save</Button>
                                    </div>
                                </Stack>
                            </form>
                        </Box>
                    </Modal>
                </Toolbar>
            </AppBar>
            <Outlet />
        </Box>
    )
}

export default Navbar;

