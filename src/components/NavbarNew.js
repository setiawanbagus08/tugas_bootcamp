import React from 'react';
import { useCommonContext } from '../contexts/CommonContext';

const NavbarNew = () => {
    const { state } = useCommonContext();

    const navbarStyle = {
        backgroundColor: state.color,
        color: 'white',
        padding: '10px',
        display: 'flex',
        justifyContent: 'space-between',
    };

    return (
        <div style={navbarStyle}>
            <div>{state.title}</div>
            <div>Other Navbar Content</div>
        </div>
    );
};

export default NavbarNew;
