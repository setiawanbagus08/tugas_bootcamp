import React, { useEffect } from 'react';
import { useCommonContext } from '../contexts/CommonContext';
import { useAuthContext } from '../contexts/AuthContext';
import { useNavigate } from 'react-router-dom'

const Page1 = () => {
    const { state, setState } = useCommonContext();
    const navigate = useNavigate();

    useEffect(() => {
        setState({ title: 'Page 1', color: 'blue' });
    }, [setState]);

    const { logout, isLoggedIn } = useAuthContext();

    const handleLogout = () => {
        logout();
    };

    useEffect(() => {
        if (!isLoggedIn) {
            navigate('/')
        }
    }, [isLoggedIn])

    return (
        <div>
            {isLoggedIn ? (
                <div>
                    <h1>{state.title}</h1>
                    <p>Content for Page 1</p>
                    <button onClick={handleLogout}>Logout</button>
                </div>
            ) : (
                <div>
                    <h1>{state.title}</h1>
                    <p>Content for Page 1</p>
                </div>
            )}
        </div>
    );
};

export default Page1;
