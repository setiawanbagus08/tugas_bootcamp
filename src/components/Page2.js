import React, { useEffect } from 'react';
import { useCommonContext } from '../contexts/CommonContext';

const Page2 = () => {
    const { state, setState } = useCommonContext();

    useEffect(() => {
        setState({ title: 'Page 2', color: 'green' });
    }, [setState]);

    return (
        <div>
            <h1>{state.title}</h1>
            <p>Content for Page 2</p>
        </div>
    );
};

export default Page2;
