export const createTheme = () => {
    const primaryColor = process.env.REACT_APP_COLOR_PRIMARY || 'blue';

    return {
        primaryColor
    };
};