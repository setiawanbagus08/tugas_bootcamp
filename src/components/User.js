import React, { useState } from "react";
import {Link} from "react-router-dom";
import { Box, Grid, TextField, Button} from "@mui/material";
import EditForm from "./EditForm";

const User = ({ users,  onEdit }) => {
    const [searchQuery, setSearchQuery] = useState('');

    const handleSearchChange = (e) => {
        setSearchQuery(e.target.value);
    };

    const number = '1';

    const filteredUsers = users.filter((user) =>
        user.name.toLowerCase().includes(searchQuery.toLowerCase())
    );

    return (
        <Box sx={{ width: 500, ml: 'auto', mr: 'auto', mt: '10px' }}>
            <TextField
                label="Search by name"
                variant="outlined"
                fullWidth
                value={searchQuery}
                onChange={handleSearchChange}
                margin="normal"
            />
            {filteredUsers.length === 0 ? (
                <div style={{
                    width: '500px',
                    marginTop: '50px'
                }}>
                    <div style={{
                        textAlign: 'center',
                        fontSize: '50px',
                        color: 'orange'
                    }}>
                        <h1>0</h1>
                    </div>
                    <div style={{
                        textAlign: 'center',
                        fontSize: '30px',
                        color: 'red'
                    }}>
                        <h2 >USER</h2>
                    </div>
                </div>
            ) : (
                <Grid container spacing={2} sx={{ marginTop: "10px" }}>
                    {filteredUsers.map((user) => {
                        return (
                            <Grid item xs={12}>
                                <div
                                    style={{
                                        display: "flex",
                                        alignItems: "center",
                                        justifyContent: "space-between",
                                        border: "1px solid black",
                                        borderRadius: "10px",
                                        paddingRight: "10px",
                                        paddingLeft: "10px"
                                    }}
                                >
                                    <div key={user.id}>
                                        <h1>{user.name}</h1>
                                        <h2>{user.address}</h2>
                                    </div>
                                    <div>
                                        <h3>{user.hobby}</h3>
                                        <div style={{display: 'flex'}}>
                                            <Button component={Link} to={`/view/${user.id}`} variant="outlined" sx={{mr: 2}}>View</Button>
                                            <EditForm user={user} onSave={onEdit} />
                                        </div>
                                    </div>
                                </div>
                            </Grid>
                        );
                    })}
                </Grid>
            )}
            
        </Box>

    )
};

export default User;