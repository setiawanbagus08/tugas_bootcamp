import  PropTypes  from "prop-types";

function UserList ({name , age}) {
    return (
        <div>
            <div>   
                Nama : {name}
            </div>
            <div>
                Umur : {age}
            </div>
        </div>
    )
};

UserList.propTypes = {
    name: PropTypes.string,
    age: PropTypes.number.isRequired
};

export default UserList;