import React from 'react';
import { useParams } from "react-router-dom";
import { Box, Grid, Input, TextField, Typography } from "@mui/material";

const View = () => {
    const { id } = useParams();
    const dataList = [
        {
            id: 1,
            name: "andi",
            hobby: 'Makan',
            address: 'Jember'
        },
        {
            id: 2,
            name: "rahmat",
            hobby: 'Makan',
            address: 'Jember'
        },
        {
            id: 3,
            name: "ratu",
            hobby: 'Makan',
            address: 'Jember'
        }
    ];

    const filterData = dataList.find((item) => item.id === parseInt(id));

    return (
        <Box sx={{ width: '500px', ml: 'auto', mr: 'auto', mt: 10, display: 'flex', flexDirection: 'column', alignItems: 'center', border: '1px solid black' }}>
            <Typography sx={{ fontSize: '40px', textAlign: 'center', fontWeight: 'bold' }}>Detail {id}</Typography>
            <Box>
                {filterData ? (
                    <div>
                        <Typography sx={{ fontSize: '30px', textAlign: 'center' }}>Name : {filterData?.name}</Typography>
                        <Typography sx={{ fontSize: '30px', textAlign: 'center' }}>Hobby : {filterData?.hobby}</Typography>
                        <Typography sx={{ fontSize: '30px', textAlign: 'center' }}>Address : {filterData?.address}</Typography>
                    </div>
                ) : (
                    <Typography>Data yang anda pilih tidak ada</Typography>
                )}
            </Box>
        </Box>
    );
};

export default View;