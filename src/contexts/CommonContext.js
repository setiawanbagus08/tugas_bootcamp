import React, { createContext, useState, useContext } from 'react';

const CommonContext = createContext();

const CommonProvider = ({ children }) => {
    const [state, setState] = useState({ title: '', color: '' });

    return (
        <CommonContext.Provider value={{ state, setState }}>
            {children}
        </CommonContext.Provider>
    );
};

const useCommonContext = () => {
    return useContext(CommonContext);
};

export { CommonProvider, useCommonContext };
